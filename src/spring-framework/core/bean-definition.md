---
title: Bean 定义
author: 会敲代码的程序猿
isOriginal: true
date: 2024-03-18
category: Spring
tag:
  - Spring
  - Spring Framework
---

# Bean 定义

## 概述

`Spring`框架的核心是`Bean`，`Bean`是`Spring`框架的基本构建块。
`Bean`是由`Spring`容器管理的对象。`Bean`是一个被实例化、组装和管理的对象。

